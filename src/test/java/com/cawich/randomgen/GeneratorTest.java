package com.cawich.randomgen;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class GeneratorTest {

    private ArrayList<Integer> checkList;

    @Before
    public void initTest() {
        checkList = new ArrayList<>();
        checkList.add(10);
        checkList.add(20);
        checkList.add(30);
        checkList.add(40);
        checkList.add(50);
        checkList.add(60);
        checkList.add(70);
    }

    @BeforeClass
    public static void beforeClass() {
        System.out.println("Before GeneratorTest.class");
    }

    @AfterClass
    public static void afterClass() {
        System.out.println("After GeneratorTest.class");
    }

    @Test(expected = Generator.RuleNotInitializedException.class)
    public void getSingleNull() throws Exception {
        (new Generator(null)).get();
    }

    @Test(expected = Generator.RuleNotInitializedException.class)
    public void getSingleNotFull() throws Exception {
        LinkedHashMap<Integer, Double> ruleNotFull = new LinkedHashMap<>();
        ruleNotFull.put(10, 0.2);
        ruleNotFull.put(20, 0.4);
        ruleNotFull.put(30, 0.175);
        ruleNotFull.put(40, 0.05);
        ruleNotFull.put(40, 0.05);
        ruleNotFull.put(60, 0.025);
        ruleNotFull.put(70, 0.1);
        Generator genNotFull = new Generator(ruleNotFull);

        genNotFull.get();
    }

    @Test(expected = Generator.RuleNotInitializedException.class)
    public void getSingleEmpty() throws Exception {
        (new Generator(new LinkedHashMap<>())).get();
    }

    @Test()
    public void getSingleNormal() throws Exception {
        LinkedHashMap<Integer, Double> rule = new LinkedHashMap<>();
        rule.put(10, 0.2);
        rule.put(20, 0.4);
        rule.put(30, 0.175);
        rule.put(40, 0.05);
        rule.put(50, 0.05);
        rule.put(60, 0.025);
        rule.put(70, 0.1);
        Generator genNormal = new Generator(rule);

        assertTrue(checkList.contains(genNormal.get()));
    }

    @Test(expected = Generator.RuleNotInitializedException.class)
    public void getSingleExtra() throws Exception {
        LinkedHashMap<Integer, Double> ruleExtra = new LinkedHashMap<>();
        ruleExtra.put(10, 0.2);
        ruleExtra.put(20, 0.4);
        ruleExtra.put(30, 0.175);
        ruleExtra.put(40, 0.5);
        ruleExtra.put(50, 0.5);
        ruleExtra.put(60, 0.25);
        ruleExtra.put(70, 0.1);
        Generator genExtra = new Generator(ruleExtra);

        genExtra.get();
    }

    @Test(expected = Generator.RuleNotInitializedException.class)
    public void getNegative() throws Exception {
        LinkedHashMap<Integer, Double> ruleNegative = new LinkedHashMap<>();
        ruleNegative.put(10, -0.2);
        ruleNegative.put(20, 0.4);
        ruleNegative.put(30, 0.175);
        ruleNegative.put(40, 0.5);
        ruleNegative.put(50, 0.5);
        ruleNegative.put(60, 0.25);
        ruleNegative.put(70, 0.1);
        Generator genNegative = new Generator(ruleNegative);

        genNegative.get();
    }

    @Test
    public void getList() throws Exception {
        LinkedHashMap<Integer, Double> rule = new LinkedHashMap<>();
        rule.put(100, 0.02);
        rule.put(700, 0.68);
        rule.put(20, 0.075);
        rule.put(50, 0.05);
        rule.put(40, 0.05);
        rule.put(60, 0.025);
        rule.put(300, 0.1);

        Generator generator = new Generator(rule);

        Map<Number, Integer> resultSum = new HashMap<>();
        for (Map.Entry<Integer, Double> entry : rule.entrySet()) {
            resultSum.put(entry.getKey(), 0);
        }

        int size = 10000;
        int count = 1000;
        try {
            for (int j = 0; j < count; j++) {
                ArrayList<Integer> results = generator.get(size);
                for (Integer i : results) {
                    resultSum.put(i, resultSum.get(i) + 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println();

        for (Map.Entry<Number, Integer> entry : resultSum.entrySet()) {
            Double cents = ((double) entry.getValue()) / count / 100;
            System.out.println(entry.getKey() + "\t->\t" + rule.get(entry.getKey()) * 100 + "\t->\t" + cents  + "\t->\t" + (rule.get(entry.getKey()) * 100 - cents));
        }
    }


}