package com.cawich.randomgen;

import java.util.*;

public class Generator {

    private Random random = new Random(System.currentTimeMillis());
    //private Random random = new Random();

    private Double DELTA = 0.0003;

    private LinkedHashMap<Integer, Double> ordered;

    public Generator(LinkedHashMap<Integer, Double> rule) {
        setRule(rule);
    }

    public Integer get() throws RuleNotInitializedException {
        Integer result = 0;
        Iterator<Map.Entry<Integer, Double>> ruleIterator = ordered.entrySet().iterator();
        double currentRandom = random.nextDouble();
        while (ruleIterator.hasNext()) {
            Map.Entry<Integer, Double> entry = ruleIterator.next();
            if (entry.getValue() >= currentRandom) {
                return entry.getKey();
            }
        }
        return result;
    }

    public ArrayList<Integer> get(int count) throws RuleNotInitializedException {
        ArrayList<Integer> result = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            result.add(get());
        }
        return result;
    }

    public void setRule(LinkedHashMap<Integer, Double> rule) {
        if (rule != null && checkProbability(rule.values())) {
            ordered = reorganizeProbabilities(rule);
        } else throw new RuleNotInitializedException();
    }

    private LinkedHashMap<Integer, Double> reorganizeProbabilities(LinkedHashMap<Integer, Double> rule) {
        LinkedHashMap<Integer, Double> result = new LinkedHashMap<>(rule);
        Map.Entry<Integer, Double> previous = null;
        Iterator<Map.Entry<Integer, Double>> ruleIterator = result.entrySet().iterator();
        while (ruleIterator.hasNext()) {
            Map.Entry<Integer, Double> entry = ruleIterator.next();
            if (previous != null) {
                entry.setValue(entry.getValue() + previous.getValue());
            }
            previous = entry;
        }
        if (previous != null)
            previous.setValue(1.0);
        return result;
    }

    /**
     * @param values all probabilities
     * @return false if sum of probabilities incorrect
     */
    private boolean checkProbability(Collection<Double> values) {
        if (values.contains(null))
            throw new RuleNotInitializedException();
        double sum = values.stream().reduce(0d, (a, b) -> a + b);
        if (sum > 1.0 - DELTA && sum < 1.0 + DELTA)
            return values.stream().allMatch(i -> i > 0);
        return false;
    }

    public class RuleNotInitializedException extends NullPointerException {
        @Override
        public String getMessage() {
            return super.getMessage();
        }
    }

}
