package com.cawich.sumsearcher;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;

/**
 * дан массив целых чисел длиной не более 10к и одно целое число - sum.
 * Выдать любую пару чисел из массива, которая в сумме дает x, либо ответить, что таких чисел нет
 */
public class Main {
    public static void main(String[] args) {
        Random r = new Random(System.currentTimeMillis());
        Integer arraySize = r.nextInt(1000);
        ArrayList<Integer> input = new ArrayList<>(arraySize);


        Integer sum = 1500 + r.nextInt(500);
        int[] pair = new int[2];
        //Integer sum = 2;

        for (int i = 0; i < arraySize; i++) {
            input.add(r.nextInt(1000));
        }
        System.out.println("\t\tour size now is: " + input.size());

        Integer max = Collections.max(input);
        boolean maxChanged = false;

        //Iterator<Integer> inputIterator = input.iterator();
        int inc = 1;
        if (max * 2 < sum) {
            System.out.println("we have no winners");
            return;
        }
        //while (inputIterator.hasNext()) {
        int size = input.size();
        for (int iter = 0; iter < size; iter ++) {
            if (maxChanged) {
                max = Collections.max(input);
                if (max * 2 < sum) {
                    //System.out.println("azaza; max*2 = " + max*2 + "; sum = " + sum);
                    return;
                }
            }
            Integer current = input.get(iter);
            if (max + current < sum) {
                input.remove(iter);
                size--;
                continue;
            }
            System.out.println("\t\t\tour size now is: " + input.size());
            int index = 0;
            for (index = input.size() - 1; index > 0; index--) {
                if ((sum - current) == input.get(index)) {
                    System.out.println(inc + ")\twe have winners: " + current + " " + input.get(index));
                    inc++;
                    break;
                    //return; // to keep 1st winner and exit
                }
            }
            if (current == max) {
                maxChanged = true;
            }
            input.remove(index);
            input.remove(iter);
            size-=2;
            //how to remove element by index here?
            //inputIterator.remove();
        }
        System.out.println("summ = " + sum);
        System.out.println("we have " + (inc == 1 ? "no winners" : (inc - 1) + " winners"));

        /**
         * 1) check max * 2 > sum
         * 2)
         */


    }
}
